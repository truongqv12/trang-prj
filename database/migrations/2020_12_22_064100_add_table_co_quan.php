<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableCoQuan extends Migration
{
    protected $tbl = "co_quan";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('co_quan')) {
            return;
        }
        Schema::create('co_quan', function (Blueprint $table) {
            $table->string('MA_CO_QUAN');
            $table->string('TEN_CO_QUAN');
            $table->primary('MA_CO_QUAN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('co_quan');
    }
}
