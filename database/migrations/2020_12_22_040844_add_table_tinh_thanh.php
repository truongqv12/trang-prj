<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableTinhThanh extends Migration
{
    protected $tbl = "tinh_thanh";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->tbl)) {
           return;
        }
        Schema::create($this->tbl, function (Blueprint $table) {
            $table->string('MA_TINH_THANH');
            $table->primary('MA_TINH_THANH');
            $table->string('TEN_TINH');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tbl);
    }
}
