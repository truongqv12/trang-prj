<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTablePhuongXa extends Migration
{
    protected $tbl = "phuong_xa";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->tbl)) {
            return;
        }
        Schema::create($this->tbl, function (Blueprint $table) {
            $table->string('MA_PHUONG_XA');
            $table->string('TEN_PHUONG_XA');
            $table->primary('MA_PHUONG_XA');
            $table->string('MA_QUAN_HUYEN_CHA');
            $table->foreign('MA_QUAN_HUYEN_CHA')->references('MA_QUAN_HUYEN')->on('QUAN_HUYEN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tbl);
    }
}
