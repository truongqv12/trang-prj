<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeHoSoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ho_so', function (Blueprint $table) {
            $table->foreign('MA_THU_TUC')->references('MA_THU_TUC')->on('THU_TUC');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ho_so', function (Blueprint $table) {
            //
        });
    }
}
