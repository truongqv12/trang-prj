<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDanhGiaTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('danh_gia')){
            return;
        }
        Schema::create('danh_gia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('MA_CO_QUAN_THUC_HIEN');
            $table->foreign('MA_CO_QUAN_THUC_HIEN')->references('MA_CO_QUAN')->on('CO_QUAN');
            $table->bigInteger('MA_HO_SO_DANH_GIA');
            $table->foreign('MA_HO_SO_DANH_GIA')->references('id')->on('HO_SO');
            $table->integer('CMND');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danh_gia');
    }
}
