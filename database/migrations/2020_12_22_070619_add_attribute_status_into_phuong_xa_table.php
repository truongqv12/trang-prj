<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeStatusIntoPhuongXaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phuong_xa', function (Blueprint $table) {
            $table->foreign('MA_QUAN_HUYEN_CHA')->references('MA_QUAN_HUYEN')->on('QUAN_HUYEN');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phuong_xa', function (Blueprint $table) {
            //
        });
    }
}
