<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableQuanHuyen extends Migration
{
    protected $tbl = "quan_huyen";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->tbl)) {
            return;
        }
        Schema::create($this->tbl, function (Blueprint $table) {
            $table->string('MA_QUAN_HUYEN');
            $table->string('TEN_QUAN_HUYEN');
            $table->primary('MA_QUAN_HUYEN');
            $table->string('MA_TINH_THANH_CHA');
            $table->foreign('MA_TINH_THANH_CHA')->references('MA_TINH_THANH')->on('TINH_THANH');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tbl);
    }
}
