<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVanBanTinTucTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('van_ban_tin_tuc')){
            return;
        }
        Schema::create('van_ban_tin_tuc', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('USER_ID');
            $table->foreign('USER_ID')->references('id')->on('users');
            $table->integer('type')->comment('0: tin tức, 1: văn bản');
            $table->string('TIEU_DE');
            $table->string('NOI_DUNG');
            $table->string('SO_KY_HIEU');
            $table->string('MA_LOAI_VAN_BAN');
            $table->foreign('MA_LOAI_VAN_BAN')->references('MA_LOAI_VAN_BAN')->on('MA_LOAI_VAN_BAN');
            $table->string('LINH_VUC');
            $table->string('FILE');
            $table->date('NGAY_BAN_HANH');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('van_ban_tin_tuc');
    }
}
