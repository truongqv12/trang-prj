<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableThuTuc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('thu_tuc')) {
            return;
        }
        Schema::create('thu_tuc', function (Blueprint $table) {
            $table->string('MA_THU_TUC');
            $table->string('TEN_THU_TUC');
            $table->primary('MA_THU_TUC');
            $table->string('MA_LINH_VUC_CHA');
            $table->foreign('MA_LINH_VUC_CHA')->references('MA_LINH_VUC')->on('LINH_VUC');

            $table->string('MA_CO_QUAN_THUC_HIEN');
            $table->foreign('MA_CO_QUAN_THUC_HIEN')->references('MA_CO_QUAN')->on('CO_QUAN');

            $table->string('CACH_THUC_THUC_HIEN');
            $table->string('TRINH_TU_THUC_HIEN');
            $table->date('THOI_HAN_GIAI_QUYET');
            $table->string('PHI');
            $table->string('LE_PHI');
            $table->string('THANH_PHAN_HO_SO');
            $table->string('YEU_CAU_DIEU_KIEN');
            $table->string('CAN_CU_PHAP_LY');
            $table->string('BIEU_MAU')->nullable();
            $table->string('KET_QUA_THUC_HIEN');
            $table->integer('MUC_DO')->comment('CÁC TRẠNG THÁI 2 3 4')	;
            $table->integer('TRANG_THAI_FILE_TEMPLATE')->comment('1:CÓ, 0: KHÔNG')	;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thu_tuc');
    }
}
