<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableLinhVuc extends Migration
{
    protected $tbl = "linh_vuc";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->tbl)) {
            return;
        }
            Schema::create($this->tbl, function (Blueprint $table) {
                $table->string('MA_LINH_VUC');
                $table->string('TEN_LINH_VUC');
                $table->primary('MA_LINH_VUC');
                $table->string('ICON');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tbl);
    }
}
