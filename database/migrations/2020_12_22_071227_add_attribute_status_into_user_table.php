<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeStatusIntoUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('WEBSITE')->nullable();
            $table->integer('CMND');
            $table->date('NGAY_CAP');
            $table->string('NOI_CAP');
            $table->integer('SO_DIEN_THOAI')->unique();
            $table->integer('SO_FAX')->nullable();
            $table->string('TINH_THANH');
            $table->foreign('TINH_THANH')->references('MA_TINH_THANH')->on('TINH_THANH');
            $table->string('QUAN_HUYEN');
            $table->string('PHUONG_XA');
            $table->string('DIA_CHI');
            $table->integer('LA_TK_DOANH_NGHIEP')->comment('1; là tk doanh nghiê[j, 0 là không');
            $table->integer('TRANG_THAI')->comment('1: hoạt động, 0 : bị block');
            $table->integer('ROLE')->comment('0: admin, 1: cán bộ, 2: bình thường');
            $table->string('HINH_ANH')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
