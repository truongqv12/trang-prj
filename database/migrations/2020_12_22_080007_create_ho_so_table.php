<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHoSoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('ho_so')) {
            return;
        }
        Schema::create('ho_so', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('NAME');
            $table->string('MA_THU_TUC');
            $table->foreign('MA_THU_TUC')->references('MA_THU_TUC')->on('THU_TUC');
            $table->bigInteger('USER_ID')->nullable();
            $table->foreign('USER_ID')->references('ID')->on('USERS');
            $table->date('NGAY_SINH');
            $table->integer('CMND');
            $table->date('NGAY_CAP');
            $table->string('NOI_CAP');
            $table->string('DAN_TOC');
            $table->string('TON_GIAO');
            $table->integer('SO_DIEN_THOAI')->unique();
            $table->integer('GIOI_TINH')->comment('0: nữ, 1: nam, 2 : khác');
            $table->string('NOI_DUNG_DE_NGHI');
            $table->integer('TRANG_THAI_HO_SO')->comment('0:đã nộp,1: đã tiếp nhận, 2: đang xử lý,3: hoàn thành,4: đã hủy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ho_so');
    }
}
