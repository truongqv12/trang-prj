<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoaiVanBanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('loai_van_ban')) {
            return;
        }
        Schema::create('loai_van_ban', function (Blueprint $table) {
            $table->string('MA_LOAI_VAN_BAN');
            $table->primary('MA_LOAI_VAN_BAN');
            $table->string('TEN_LOAI_VAN_BAN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loai_van_ban');
    }
}
